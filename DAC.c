 
void Dac()
{
  dac.setVoltage((1 * 4095) / 5, false);    //Set voltage to 1V
  delay(2000);
  dac.setVoltage((2 * 4095) / 5, false);    //Set voltage to 2V
  delay(2000);
  dac.setVoltage((3 * 4095) / 5, false);    //Set voltage to 3V
  delay(2000);
  dac.setVoltage((4 * 4095) / 5, false);    //Set voltage to 4V
  delay(2000);
  dac.setVoltage(4095, false);              //Set voltage to 5V or (Vcc)
  delay(2000);
}